# PIV_pump_planes


The following repository contains the PIV averaged results discussed in [**"Particle Image Velocimetry In A Centrifugal Pump: Influence Of Walls On The Flow At Different Axial Positions"**](doi.org/10.1115/1.4063616). 
The PIV results at different planes within the pump impeller geometry are available in different formats, together with scripts illustrating how to read and plot the data.
In addition, for those interested in performing CFD simulations, the pump geometry is available in various CAD formats.


## Pump_Drawings

This folder contains the pump prototype model made in Solid Edge 2021 academic version in `.par` and `.asm` formats.
In addition, the pump CAD geometry is also available in `.igs` and `.stp` (STEP) formats.


## Velocity_Fields

This folder contains the PIV ensemble mean results in the three different plane studies in XXXXXX.
The files are in `.npz` format, created with the `np.savez` function of `numpy`.
They are located in different folders, named as follows

`{plane_position}-{pump_speed}rpm-{mass_flow_rate}_kgh`,

where
*  `plane_position` refers to the PIV acquisition plane (`HUB`, `CENTER` or `SHROUD`)
*  pump_speed refers to the pump speed of the experiment 
*  mass_flow_rate refers to the mass flow rate of the experiment.


## scripts:

This folder contains `.py' files:
* `read_results_and_plot.py`: illustrates how to read the `.npz` files from the `Velocity_Fields` folder. 
* `from_npz_to_xlsx.py`: Scripts to convert the `.npz` files to the `.xlsx` (MS Excel) format.
* `from_npz_to_csv.py`: Scripts used to convert the `.npz` files to the `.csv` (Comma-Separated Values) format.
* `from_npz_to_mat.py`: Scripts used to convert the `.npz` files to the `.mat` (MATLAB) format.

In addition, the PIV results are readily available in the different formats.


## Reference

The related article can be found at [doi.org/10.1115/1.4063616](doi.org/10.1115/1.4063616).
