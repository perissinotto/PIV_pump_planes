import numpy as np
import matplotlib.pyplot as plt
import os
from pathlib import Path
import cv2

plt.rcParams['image.cmap'] = 'jet'
plt.rcParams.update({'font.size': 12})
plt.rcParams.update({
    "text.usetex": False,
    "font.family": "serif",
})

# Location where the experiments are stored
exps_folder = "../Velocity_Fields/"

# Create an output folder to store the plots
output_folder_plots = Path('plots')
output_folder_plots.mkdir(exist_ok=True, parents=True)

# Loading the experiment folders
experiments = os.listdir(exps_folder)
plot_experiments = []
for experiment in experiments:
    if not os.path.isdir(os.path.join(exps_folder, experiment)):
        continue
    plot_experiments.append(experiment)

# Sorting experiments by flow rate, you may change for different analyses
plot_experiments = sorted(plot_experiments, key=lambda x: int(x.split("_kgh")[0].split("_")[-1]), reverse=True)

# Looping over the selected experiments
for experiment in plot_experiments:
    print("Processing/Plotting experiment %s!" % experiment)

    # Defining the file location
    plot_file = os.path.join(exps_folder, '%s/res/results_impeller.npz' % experiment)  # for impeller fields

    # Retrieving experiment name
    experiment_name = plot_file.split('/')[-3]

    # Loading the results
    RESULT_FILE = np.load(plot_file)

    # Reading the results
    X = RESULT_FILE['X']
    Y = RESULT_FILE['Y']
    U = RESULT_FILE['U']
    V = RESULT_FILE['V']
    Urms = RESULT_FILE['Urms']
    Vrms = RESULT_FILE['Vrms']
    UVrms = RESULT_FILE['UVrms']

    # Image mask
    img_mask = RESULT_FILE['img_mask']
    img_mask_true_resolution = cv2.resize(img_mask, (X.shape[1], X.shape[0]))
    geom_flag = img_mask > 10

    # Calculating the velocity magnitude
    U_mag = np.sqrt(U ** 2.0 + V ** 2.0)

    # Calculating the TKE
    TKE = 0.5 * (Urms ** 2.0 + Vrms ** 2.0)

    # Default values
    # When using th 'nanmin/nanmax' options, the contours max and min are
    # manually defined.
    # You may want to manually override the contour colours, as commented below
    u_lim = [np.nanmin(U), np.nanmax(U)]
    v_lim = [np.nanmin(V), np.nanmax(V)]
    urms_lim = [np.nanmin(Urms), np.nanmax(Urms)]
    vrms_lim = [np.nanmin(Vrms), np.nanmax(Vrms)]
    uvrms_lim = [np.nanmin(UVrms), np.nanmax(UVrms)]
    TKE_lim = [np.nanmin(TKE), np.nanmax(TKE)]
    TKE_lim = [np.nanmin(TKE), 0.12]
    velmag_lim = np.sqrt(u_lim[1] ** 2.0 + v_lim[1] ** 2.0)

    # Applying the image/pump mask into the fields
    img_mask_interpolated = cv2.resize(img_mask, (X.shape[1], X.shape[0]))
    geom_flag = img_mask_interpolated > 10
    U[geom_flag] = np.nan
    V[geom_flag] = np.nan
    U_mag[geom_flag] = np.nan
    TKE[geom_flag] = np.nan

    ##### Quiver/Vector plot example
    # Creating a simple quiver (vector) plot
    ax = plt.gca()
    ax.set_aspect('equal', 'box')
    ax.set_xlim(75, 200)
    ax.set_ylim(35, 160)
    ax.axis('off')
    quiv = ax.quiver(X, Y, U, V, U_mag, units='xy', scale_units=None, pivot='mid', scale=0.2, clim=(0, velmag_lim))
    cbar = plt.colorbar(quiv, ax=ax, shrink=0.70)
    cbar.ax.set_xlabel(r'$| \vec{u} |$ [m/s]')
    ax.set_xlabel('$x$ [mm]')
    ax.set_ylabel('$y$ [mm]')
    # Saving the simple quiver (vector) plot
    plt.tight_layout()
    plt.savefig(Path(output_folder_plots, 'example_%s_quiv.png' % experiment_name), dpi=400, bbox_inches='tight')
    plt.close()

    ##### Contour plot example
    # Retrieving the axis properties
    ax = plt.gca()
    ax.set_xlim(75, 200)
    ax.set_ylim(35, 160)
    ax.set_aspect('equal', 'box')
    ax.axis('off')
    pmesh = ax.pcolormesh(X, Y, TKE, vmin=0.0, vmax=0.1, shading='gourad')
    cbar = plt.colorbar(pmesh, ax=ax, shrink=0.70)
    cbar.ax.set_xlabel(r'$| {TKE} |$ [m$^2$/s$^2$ ]')

    # Saving the simple contour plot
    plt.tight_layout()
    plt.savefig(Path(output_folder_plots, 'example_%s_rms.png' % experiment_name), dpi=400, bbox_inches='tight')
    plt.close()